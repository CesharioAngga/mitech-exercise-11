<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Make Payment</name>
   <tag></tag>
   <elementGuidId>0e1304a7-c03f-4b4f-8662-50645f9216fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div/div[3]/div[2]/div/div/div[1]/div[1]/a[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-success.btn-sm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d679feb5-6190-413a-bb6f-10a2efe41190</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-success btn-sm</value>
      <webElementGuid>7ed7fffb-4ad8-44d0-8835-55bf75f410e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>d36c098a-1ff6-4528-aab3-ea0286f48efd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Make Payment</value>
      <webElementGuid>f094fb39-f590-4226-8748-afa26b7b1585</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;menu-position-side menu-side-left full-screen&quot;]/div[@class=&quot;all-wrapper solid-bg-all&quot;]/div[@class=&quot;layout-w&quot;]/div[@class=&quot;content-w&quot;]/div[@class=&quot;content-i&quot;]/div[@class=&quot;content-box&quot;]/div[@class=&quot;element-wrapper compact pt-4&quot;]/div[@class=&quot;element-actions&quot;]/a[@class=&quot;btn btn-success btn-sm&quot;]</value>
      <webElementGuid>4eb21abf-3856-4557-b248-dceacfaf5f6f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Account'])[1]/following::a[1]</value>
      <webElementGuid>6732fcc9-1059-40bb-ad4a-77c82be2821b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mortgages'])[1]/following::a[2]</value>
      <webElementGuid>85a7c617-07f7-4c85-a3e6-5a8d93ee24fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your nearest branch closes in: 30m 5s'])[1]/preceding::a[1]</value>
      <webElementGuid>a283e806-577e-4407-a031-d8d94d0e761d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[16]</value>
      <webElementGuid>421d9e5a-55a7-400a-beb8-ffbe71685859</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/div/div/div/a[2]</value>
      <webElementGuid>7329350d-23d5-4106-bcc1-1d5edda79b8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = 'Make Payment' or . = 'Make Payment')]</value>
      <webElementGuid>1e01bd8a-3e73-4e4c-af56-6cbc20d77bb0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
