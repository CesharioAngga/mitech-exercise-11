<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Add Account</name>
   <tag></tag>
   <elementGuidId>22b8fe54-7294-4355-802e-d8c8828aa40e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div/div[3]/div[2]/div/div/div[1]/div[1]/a[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-primary.btn-sm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>866ee048-7ade-4e88-97b4-7d4ff29f0fbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-sm</value>
      <webElementGuid>486488c0-e1af-4764-85a8-5b960a0fb678</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>d27746cf-3066-4e1e-b7c4-1dcac944cffb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add Account</value>
      <webElementGuid>99da9d0d-8180-4701-afbb-6efe55536ca8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;menu-position-side menu-side-left full-screen&quot;]/div[@class=&quot;all-wrapper solid-bg-all&quot;]/div[@class=&quot;layout-w&quot;]/div[@class=&quot;content-w&quot;]/div[@class=&quot;content-i&quot;]/div[@class=&quot;content-box&quot;]/div[@class=&quot;element-wrapper compact pt-4&quot;]/div[@class=&quot;element-actions&quot;]/a[@class=&quot;btn btn-primary btn-sm&quot;]</value>
      <webElementGuid>4658d676-628f-4576-8c91-ae17ba53ac6b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mortgages'])[1]/following::a[1]</value>
      <webElementGuid>0b1f0c4c-d1a3-4897-8a72-99aac26e687b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Loans'])[1]/following::a[2]</value>
      <webElementGuid>2304b8fe-e793-4b77-8975-aa8cf178c273</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Payment'])[1]/preceding::a[1]</value>
      <webElementGuid>e0d8ed37-645e-494c-b084-d3ae72b4baaa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[15]</value>
      <webElementGuid>e39cdc5c-3257-4ad0-a8f0-7cab64625520</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[2]/div/div/div/div/a</value>
      <webElementGuid>af82adf2-ee7f-4545-be71-7e982c9bcbca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = 'Add Account' or . = 'Add Account')]</value>
      <webElementGuid>bbb8efe6-db39-47f4-b904-ab29783e32f1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
