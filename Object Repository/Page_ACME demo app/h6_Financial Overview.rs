<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h6_Financial Overview</name>
   <tag></tag>
   <elementGuidId>74554b55-1a46-45d2-a35e-2236c80c2a8f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Your nearest branch closes in: 30m 5s'])[1]/following::h6[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h6.element-header</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>3ff6006e-8a21-4566-a3ac-d5ac152878be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>element-header</value>
      <webElementGuid>30365582-6dba-49be-ae9d-b1eb8b001341</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Financial Overview
              </value>
      <webElementGuid>f50bf3fd-9cba-47fd-8927-7493ecd99ed2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;menu-position-side menu-side-left full-screen&quot;]/div[@class=&quot;all-wrapper solid-bg-all&quot;]/div[@class=&quot;layout-w&quot;]/div[@class=&quot;content-w&quot;]/div[@class=&quot;content-i&quot;]/div[@class=&quot;content-box&quot;]/div[@class=&quot;element-wrapper compact pt-4&quot;]/h6[@class=&quot;element-header&quot;]</value>
      <webElementGuid>45714d28-4b5e-4cd6-b434-7a5a2e810666</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your nearest branch closes in: 30m 5s'])[1]/following::h6[1]</value>
      <webElementGuid>a9debb57-8b09-47a0-ad66-7de7102f3353</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Payment'])[1]/following::h6[2]</value>
      <webElementGuid>77037554-a4d4-4e32-94b9-37e675da424b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Balance'])[1]/preceding::h6[1]</value>
      <webElementGuid>b73bc838-a029-43f1-80b1-5759f1d769cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$350'])[1]/preceding::h6[1]</value>
      <webElementGuid>6c14bac8-4cbc-4c1e-9bc3-558f46c76c38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Financial Overview']/parent::*</value>
      <webElementGuid>894353a5-6c9d-42a0-a325-b24d0565b82c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/h6[2]</value>
      <webElementGuid>83eda7a8-e32a-43a7-a028-6b7ae52acfa5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = '
                Financial Overview
              ' or . = '
                Financial Overview
              ')]</value>
      <webElementGuid>6da0f29d-f6a3-47cb-a090-ed044139a823</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
